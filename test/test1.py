import tensorflow as tf
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

with tf.device('/cpu:0'):
    with tf.variable_scope('scope1', caching_device='/cpu:0'):
        a = tf.get_variable('v1', [])
        #a = tf.Variable(0, [])
        #print(tf.get_variable_scope())


with tf.variable_scope('scope2', caching_device='/cpu:0'):
    b = tf.get_variable('v1', [])
    #b = tf.Variable(0, [])
    #print(tf.get_variable_scope())


for v in tf.trainable_variables():
    print(v.name, v.device)

sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
sess.close()