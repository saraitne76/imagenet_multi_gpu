import tensorflow as tf


def parse_train(example_proto, crop_size):
    features = {'label': tf.FixedLenFeature([], tf.int64, default_value=0),
                'height': tf.FixedLenFeature([], tf.int64, default_value=0),
                'width': tf.FixedLenFeature([], tf.int64, default_value=0),
                'image': tf.FixedLenFeature([], tf.string, default_value=''),
                'file_name': tf.FixedLenFeature([], tf.string, default_value='')}
    parsed_features = tf.parse_single_example(example_proto, features)

    h = tf.to_int32(parsed_features['height'])
    w = tf.to_int32(parsed_features['width'])
    img = tf.decode_raw(parsed_features['image'], tf.uint8)
    img = tf.reshape(img, (h, w, 3))
    ratio = tf.divide(tf.maximum(h, w), tf.minimum(h, w))
    new_shoter = tf.random_uniform([], 256, 481, dtype=tf.int32)
    new_longer = tf.to_int32(tf.multiply(tf.cast(new_shoter, tf.float64), ratio))
    img = tf.cond(tf.less(w, h),
                  lambda: tf.image.resize_images(img, (new_longer, new_shoter)),
                  lambda: tf.image.resize_images(img, (new_shoter, new_longer)))

    img = tf.random_crop(img, crop_size)
    # img = tf.image.random_flip_left_right(img)
    # img = tf.image.random_brightness(img, 0.15)
    # img = tf.image.random_contrast(img, lower=0.6, upper=1.8)
    # img = tf.image.random_hue(img, 0.08)
    # img = tf.image.random_saturation(img, lower=0.2, upper=2.0)
    img = tf.divide(tf.cast(img, tf.float32), 255.)
    img = tf.image.per_image_standardization(img)
    label = tf.cast(parsed_features['label'], tf.int32)
    return img, label-1


def parse_eval(example_proto, crop_size):
    features = {'label': tf.FixedLenFeature([], tf.int64, default_value=0),
                'height': tf.FixedLenFeature([], tf.int64, default_value=0),
                'width': tf.FixedLenFeature([], tf.int64, default_value=0),
                'image': tf.FixedLenFeature([], tf.string, default_value=''),
                'file_name': tf.FixedLenFeature([], tf.string, default_value='')}
    parsed_features = tf.parse_single_example(example_proto, features)

    h = tf.to_int32(parsed_features['height'])
    w = tf.to_int32(parsed_features['width'])
    img = tf.decode_raw(parsed_features['image'], tf.uint8)
    img = tf.reshape(img, (h, w, 3))
    ratio = tf.divide(tf.maximum(h, w), tf.minimum(h, w))
    scale1 = _10_crop_func(img, h, w, ratio, 224, crop_size)
    scale2 = _10_crop_func(img, h, w, ratio, 256, crop_size)
    scale3 = _10_crop_func(img, h, w, ratio, 384, crop_size)
    scale4 = _10_crop_func(img, h, w, ratio, 480, crop_size)
    scale5 = _10_crop_func(img, h, w, ratio, 640, crop_size)

    multi_scale_crop = tf.concat([scale1, scale2, scale3, scale4, scale5], axis=0)

    label = tf.cast(parsed_features['label'], tf.int32)
    return multi_scale_crop, tf.expand_dims(label-1, 0)


def _10_crop_func(img, w, h, ratio, resize, crop_size):
    new_shoter = resize
    new_longer = tf.to_int32(tf.multiply(tf.cast(new_shoter, tf.float64), ratio))
    new_h = tf.cond(tf.less(w, h), lambda: new_longer, lambda: new_shoter)
    new_w = tf.cond(tf.less(w, h), lambda: new_shoter, lambda: new_longer)
    img = tf.image.resize_images(img, (new_h, new_w))
    img = tf.divide(tf.cast(img, tf.float32), 255.)
    img = tf.image.per_image_standardization(img)

    img_center = tf.image.crop_to_bounding_box(img,
                                               new_h // 2 - crop_size[0] // 2, new_w // 2 - crop_size[1] // 2,
                                               crop_size[0], crop_size[1])
    img_top_left = tf.image.crop_to_bounding_box(img,
                                                 0, 0, crop_size[0], crop_size[1])
    img_top_right = tf.image.crop_to_bounding_box(img,
                                                  0, new_w - crop_size[1], crop_size[0], crop_size[1])
    img_bot_left = tf.image.crop_to_bounding_box(img,
                                                 new_h - crop_size[0], 0, crop_size[0], crop_size[1])
    img_bot_right = tf.image.crop_to_bounding_box(img,
                                                  new_h - crop_size[0], new_w - crop_size[1], crop_size[0],
                                                  crop_size[1])
    _10_crop = tf.stack([img_center,
                         tf.image.flip_left_right(img_center),
                         img_top_left,
                         tf.image.flip_left_right(img_top_left),
                         img_top_right,
                         tf.image.flip_left_right(img_top_right),
                         img_bot_left,
                         tf.image.flip_left_right(img_bot_left),
                         img_bot_right,
                         tf.image.flip_left_right(img_bot_right)])
    return _10_crop


def train_dataset(tfrecord, input_size, batch_size, prefetch):
    files = tf.data.Dataset.list_files(tfrecord)
    dataset = files.apply(tf.contrib.data.parallel_interleave(tf.data.TFRecordDataset,
                                                              cycle_length=12,
                                                              sloppy=True,
                                                              buffer_output_elements=prefetch,
                                                              prefetch_input_elements=prefetch))
    dataset = dataset.map(lambda x: parse_train(x, input_size), num_parallel_calls=12)
    dataset = dataset.shuffle(buffer_size=prefetch)
    dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(prefetch)
    # dataset = dataset.repeat()
    iter = dataset.make_initializable_iterator()

    # dataset = tf.data.TFRecordDataset(tfrecord)
    # dataset = dataset.map(lambda x: parse_train(x, input_size), num_parallel_calls=6)
    # dataset = dataset.shuffle(buffer_size=prefetch)
    # dataset = dataset.batch(batch_size)
    # dataset = dataset.prefetch(prefetch)
    # # dataset = dataset.repeat()
    # iter = dataset.make_initializable_iterator()
    # #X, Y = iter.get_next()
    return iter


def eval_dataset(tfrecord, input_size, prefetch):
    dataset = tf.data.TFRecordDataset(tfrecord)
    dataset = dataset.map(lambda x: parse_eval(x, input_size), num_parallel_calls=12)
    # dataset = dataset.shuffle(buffer_size=batch_size)
    # dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(prefetch)
    # dataset = dataset.repeat()
    iter = dataset.make_initializable_iterator()
    #X, Y = iter.get_next()
    return iter
