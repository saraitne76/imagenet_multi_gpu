import cPickle
import numpy as np
import cv2
import os

train_data = []
train_label = []

for i in range(1, 6):
    with open('/home/swlee/data/cifar-10-batches-py/data_batch_%d' % i) as f:
        dict = cPickle.load(f)
        train_data.append(np.float32(dict['data']))
        train_label.append(np.float32(dict['labels']))


with open('/home/swlee/data/cifar-10-batches-py/test_batch') as f:
    dict=cPickle.load(f)
    test_data = np.float32(dict['data'])
    test_label = np.float32(dict['labels'])

train_data = np.float32(np.concatenate(train_data))
train_data = (train_data.reshape(50000, 3, 32, 32)).transpose([0, 2, 3, 1])
train_label = np.concatenate(train_label)

test_data = (test_data.reshape(10000, 3, 32, 32)).transpose([0, 2, 3, 1])

for i, img in enumerate(train_data):
    path = os.path.join('/home/swlee/data/cifar10_data/train', str(int(train_label[i])))
    if not os.path.isdir(path):
        os.mkdir(path)
    cv2.imwrite(os.path.join(path, '%.5d.JPEG' % i), img[..., [2, 1, 0]])

for i, img in enumerate(test_data):
    path = os.path.join('/home/swlee/data/cifar10_data/val', str(int(test_label[i])))
    if not os.path.isdir(path):
        os.mkdir(path)
    cv2.imwrite(os.path.join(path, '%.5d.JPEG' % i), img[..., [2, 1, 0]])