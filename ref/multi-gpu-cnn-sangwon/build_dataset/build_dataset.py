from datetime import datetime
import os
import random
import cv2
import soundfile
import python_speech_features
import sys
import threading

import numpy as np
import tensorflow as tf

tf.app.flags.DEFINE_string('train_directory', '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development',
                           'Training data directory')
tf.app.flags.DEFINE_string('validation_directory', '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development',
                           'Validation data directory')

tf.app.flags.DEFINE_string('output_directory', '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development/tfr',
                           'TFrecord output directory')

tf.app.flags.DEFINE_integer('train_shards', 10,
                            'Number of shards in training TFRecord files.')
tf.app.flags.DEFINE_integer('validation_shards', 5,
                            'Number of shards in validation TFRecord files.')

tf.app.flags.DEFINE_integer('num_threads', 5,
                            'Number of threads to preprocess the images.')
tf.app.flags.DEFINE_string('train_labels_file',
                           '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development/evaluation_setup/fold1_train.txt',
                           'Labels file. if not specified'
                           'the directory structure is automatically used for label')
tf.app.flags.DEFINE_string('validation_labels_file',
                           '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development/evaluation_setup/fold1_evaluate.txt',
                           'Labels file. if not specified'
                           'the directory structure is automatically used for label')

FLAGS = tf.app.flags.FLAGS


def int_feature(value):
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _convert_to_example(filename, image_buffer, label, text, shape):
    image_format = 'JPEG'

    example = tf.train.Example(features=tf.train.Features(feature={
        # 'data/colorspace': bytes_feature(tf.compat.as_bytes(colorspace)),
        'data/class/label': int_feature(label),
        'data/class/text': bytes_feature(tf.compat.as_bytes(text)),
        'data/format': bytes_feature(tf.compat.as_bytes(image_format)),
        'data/shape': bytes_feature(tf.compat.as_bytes(shape)),
        'data/encoded': bytes_feature(tf.compat.as_bytes(image_buffer)),
    }))
    return example


def process_data(filename):
    extension = filename[-3:].lower()

    if (extension == 'png') or (extension == 'jpg') or (extension == 'peg'):
        #handling img file format
        # data = np.asarray(Image.open(filename))
        data = cv2.imread(filename, cv2.IMREAD_COLOR)
        data_bin = np.ndarray.tostring(data)

    elif extension == 'wav':
        #handling sound file format
        sig, fs = soundfile.read(filename, dtype='float32')

        if len(sig.shape)>1:
            sig = np.average(sig, 1)

        winlen = 0.08
        winstep = 0.04
        nfilt=250

        frame_len = np.int(np.float(fs)*winlen)
        nfft = np.int(2**np.ceil(np.log2(frame_len)))
        data = python_speech_features.logfbank(sig, fs, winlen, winstep, nfilt, nfft)
        data = np.asarray(data)
        data_bin = np.ndarray.tostring(data)

    else:
        print('not supported file extension: '+extension)
        exit()

    data_shape = np.ndarray.tostring(np.asarray(data.shape))
    return data_bin, data_shape


def build_shards(thread_index, ranges, shardname, filenames,
                               texts, labels, num_shards):
    num_threads = len(ranges)
    assert not num_shards % num_threads
    num_shards_per_thread = int(num_shards / num_threads)

    shard_ranges = np.linspace(ranges[thread_index][0],
                             ranges[thread_index][1],
                             num_shards_per_thread + 1).astype(int)
    num_files_in_thread = ranges[thread_index][1] - ranges[thread_index][0]

    counter = 0
    for s in range(num_shards_per_thread):
        shard = thread_index * num_shards_per_thread + s
        output_filename = '%s-%.4d-of-%.4d' % (shardname, shard + 1, num_shards)
        output_file = os.path.join(FLAGS.output_directory, output_filename)
        if not os.path.isdir(FLAGS.output_directory):
            os.mkdir(FLAGS.output_directory)
        writer = tf.python_io.TFRecordWriter(output_file)
        # bin_writer = open(output_file+'.bin', 'wb')

        shard_counter = 0
        files_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
        for i in files_in_shard:
            filename = filenames[i]
            label = labels[i]
            text = texts[i]

            try:
                image_buffer, shape = process_data(filename)
            except Exception as e:
                print(e)
                print('SKIPPED: Unexpected error while decoding %s.' % filename)
                continue

            example = _convert_to_example(filename, image_buffer, label,
                                        text, shape)
            writer.write(example.SerializeToString())
            # bin_writer.write(image_buffer)
            shard_counter += 1
            counter += 1

            if not counter % 500:
                print('%s [thread %d]: Processed %d of %d images in thread batch.' %
                     (datetime.now(), thread_index, counter, num_files_in_thread))
                sys.stdout.flush()

    writer.close()
    # bin_writer.close()
    print('%s [thread %d]: Wrote %d images to %s' %
          (datetime.now(), thread_index, counter, output_file))
    sys.stdout.flush()
    shard_counter = 0
    # print('%s [thread %d]: Wrote %d images to %d shards.' %
    #         (datetime.now(), thread_index, counter, thread_index))
    sys.stdout.flush()


def build_processing_threads(shardname, filenames, texts, labels, num_shards):
    assert len(filenames) == len(texts)
    assert len(filenames) == len(labels)

    # Break all images into batches with a [ranges[i][0], ranges[i][1]].
    spacing = np.linspace(0, len(filenames), FLAGS.num_threads + 1, dtype=np.int64)
    ranges_per_thread = []
    for i in range(len(spacing) - 1):
        ranges_per_thread.append([spacing[i], spacing[i + 1]])

    # Launch a thread for each batch.
    print('Launching %d threads for spacings: %s' % (FLAGS.num_threads, ranges_per_thread))
    sys.stdout.flush()

    coord = tf.train.Coordinator()
    threads = []
    for thread_index in range(len(ranges_per_thread)):
        args = (thread_index, ranges_per_thread, shardname, filenames,
                texts, labels, num_shards)
        t = threading.Thread(target=build_shards, args=args)
        t.start()
        print('%d thread is launched' % (thread_index))
        threads.append(t)

    # Wait for all the threads to terminate.
    coord.join(threads)
    print('%s: Finished writing all %d images in data set.' %
        (datetime.now(), len(filenames)))
    sys.stdout.flush()


def build_metadata_from_dataset(data_dir):
    unique_labels = os.listdir(data_dir)

    labels = []
    filenames = []
    texts = []

    label_index = -1

    for label in unique_labels:
        file_path = os.path.join(data_dir, label)
        matching_files = os.listdir(file_path)
        matching_files = [os.path.join(file_path, x) for x in matching_files]
        # print(len(matching_files), file_path)

        labels.extend([label_index] * len(matching_files))
        texts.extend([label] * len(matching_files))
        filenames.extend(matching_files)

        if not label_index % 100:
            print('Finished finding files in %d of %d classes.' % (
                label_index, len(unique_labels)))
        label_index += 1

    shuffled_index = list(range(len(filenames)))
    random.seed(1)
    random.shuffle(shuffled_index)

    filenames = [filenames[i] for i in shuffled_index]
    texts = [texts[i] for i in shuffled_index]
    labels = [labels[i] for i in shuffled_index]

    print('Found %d files across %d labels inside %s.' %
        (len(filenames), len(unique_labels), data_dir))
    return filenames, texts, labels


def _is_csv(labels_file):
    return labels_file[-3:]=='csv'


def get_metadata_from_file(labels_file):
    lines = open(labels_file, 'r').readlines()
    unique_labels = []

    labels = []
    filenames = []
    texts = []

    label_index = -1

    for line in lines:
        filename, text = line.split()
        if not text in unique_labels:
            unique_labels.append(text)
            label_index+=1
        label = label_index

        filenames.append(os.path.join(FLAGS.train_directory, filename))
        texts.append(text)
        labels.append(label)

    shuffled_index = list(range(len(filenames)))
    random.seed(1)
    random.shuffle(shuffled_index)

    filenames = [filenames[i] for i in shuffled_index]
    texts = [texts[i] for i in shuffled_index]
    labels = [labels[i] for i in shuffled_index]

    print('Found %d files across %d labels.' %
        (len(filenames), len(unique_labels)))
    return filenames, texts, labels


def _process_dataset(name, directory, num_shards, labels_file):
    if labels_file is None:
        filenames, texts, labels = build_metadata_from_dataset(directory)
    else:
        if _is_csv(labels_file):
            pass
        else:
            filenames, texts, labels = get_metadata_from_file(labels_file)

    build_processing_threads(name, filenames, texts, labels, num_shards)


def main(unused_argv):
    assert not FLAGS.train_shards % FLAGS.num_threads, (
      'Please make the FLAGS.num_threads commensurate with FLAGS.train_shards')
    assert not FLAGS.validation_shards % FLAGS.num_threads, (
      'Please make the FLAGS.num_threads commensurate with '
      'FLAGS.validation_shards')
    print('Saving results to %s' % FLAGS.output_directory)

    _process_dataset('validation', FLAGS.validation_directory,
                    FLAGS.validation_shards, FLAGS.validation_labels_file)
    _process_dataset('train', FLAGS.train_directory,
                    FLAGS.train_shards, FLAGS.train_labels_file)


if __name__ == '__main__':
  tf.app.run()
