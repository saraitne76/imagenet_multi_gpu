import tensorflow as tf
from tensorflow.python.training import moving_averages

BATCHNORM_UPDATE_COLLECTION = 'update_ops'

def variable_on_cpu(name,
                    shape=None,
                    initializer=None,
                    dtype=tf.float32,
                    regularizer=None,
                    trainable=True,
                    collections=None,
                    reuse=None):
    with tf.device('/cpu:0'):
        var = tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer,
                              regularizer=regularizer, trainable=trainable,
                              caching_device='/cpu:0', use_resource=True)
        # print var
    return var


def bn_relu(inputs,
            is_training,
            decay=0.997,
            epsilon=1e-5,
            center=True,
            scale=True,
            name=None,
            reuse=None):
    inputs = tf.layers.batch_normalization(inputs, training=is_training,
                            momentum=decay, epsilon=epsilon, center=center, scale=scale, reuse=reuse, name=name)
    inputs = tf.nn.relu(inputs)

    tf.summary.scalar('sparsity', tf.nn.zero_fraction(inputs))
    return inputs


def dcr(inputs, drop_rate=0.5):
    inputs = tf.transpose(inputs, [0, 3, 1, 2])
    index = tf.reduce_mean(inputs, axis=[2, 3])
    C = inputs.get_shape().as_list()[1]
    img_size = inputs.get_shape().as_list()[-2]
    k = int(C * drop_rate)

    val, indices = tf.nn.top_k(index, k=k)
    inputs = tf.reshape(inputs, [-1, img_size, img_size])
    indices = tf.one_hot(indices, C)
    mask = tf.cast(tf.reduce_sum(indices, axis=1), tf.bool)
    mask = tf.reshape(mask, [-1])
    masked = tf.boolean_mask(inputs, mask)
    inputs = tf.transpose(tf.reshape(masked, [-1, k, img_size, img_size]), [0, 2, 3, 1])
    print indices
    return inputs, indices


def channel_dropout(inputs, keep_prob):
    mask = tf.reduce_mean(inputs, axis=[1, 2], keep_dims=True)
    mask = tf.clip_by_value(mask, 1, 1)
    mask = tf.nn.dropout(mask, keep_prob)
    return tf.multiply(inputs, mask)