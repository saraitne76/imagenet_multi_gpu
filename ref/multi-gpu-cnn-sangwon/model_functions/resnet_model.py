import tensorflow as tf
import utils

WEIGHT_DECAY = 0

def block_layer(inputs, filters, block_fn, blocks, strides, is_training, name):
    filters_out = filters*4 if block_fn is bottleneck_block else filters
    with tf.variable_scope(name):
        def projection_shortcut(inputs):
            return tf.layers.conv2d(
                inputs=inputs, filters=filters_out, kernel_size=1,
                strides=strides, name='proj')

        inputs = block_fn(inputs, filters, is_training, projection_shortcut, strides, 'residual_block_0')
        for i in range(1, blocks):
            inputs = block_fn(inputs, filters, is_training, None, 1, 'residual_block_%d' % (i))

    return inputs

def building_block(inputs, filters, is_training, projection_shortcut, strides, name):
    with tf.variable_scope(name):
        shortcut = inputs
        inputs = utils.bn_relu(inputs, is_training)
        if projection_shortcut is not None:
            shortcut = projection_shortcut(inputs)
        inputs = tf.layers.conv2d(inputs=inputs, filters=filters, kernel_size=3,
                                  strides=strides, padding='SAME', name='conv_1st')

        inputs = utils.bn_relu(inputs, is_training)
        inputs = tf.layers.conv2d(inputs=inputs, filters=filters, kernel_size=3,
                                  strides=1, padding='SAME', name='conv_2nd')

    return inputs + shortcut

def reduced_building_block(inputs, filters, is_training, projection_shortcut, strides, name):
    with tf.variable_scope(name):
        shortcut = inputs
        inputs = utils.bn_relu(inputs, is_training)
        if projection_shortcut is not None:
            shortcut = projection_shortcut(inputs)
        inputs = utils.active_conv2d(inputs=inputs, filters=filters, kernel_size=3,
                                  strides=strides, padding='SAME', name='conv_1st')

        inputs = utils.bn_relu(inputs, is_training)
        inputs = utils.active_conv2d(inputs=inputs, filters=filters, kernel_size=3,
                                  strides=1, padding='SAME', name='conv_2nd')

    return inputs + shortcut

def bottleneck_block(inputs, filters, is_training, projection_shortcut,
                     strides, name):
    with tf.variable_scope(name):
        shortcut = inputs
        inputs = utils.bn_relu(inputs, is_training, name='1st')

        if projection_shortcut is not None:
            shortcut = projection_shortcut(inputs)

        inputs = tf.layers.conv2d(inputs=inputs, filters=filters, kernel_size=1,
                                  strides=1, padding='SAME', name='conv_1st')

        mask = tf.reduce_mean(inputs, axis=[1, 2], keepdims=True)
        mask = tf.clip_by_value(mask, 1, 1)
        mask = tf.nn.dropout(mask, 0.7 if is_training else 1.)
        inputs = tf.multiply(inputs, mask)

        inputs = utils.bn_relu(inputs, is_training)
        inputs = tf.layers.conv2d(inputs=inputs, filters=filters, kernel_size=3,
                                  strides=strides, padding='SAME', name='conv_2nd')

        inputs = utils.bn_relu(inputs, is_training)
        inputs = tf.layers.conv2d(inputs=inputs, filters=filters*4, kernel_size=1,
                                  strides=1, padding='SAME', name='conv_3rd')

    return inputs + shortcut

def inference(inputs, num_classes, is_training, params=None):
    # resnet_size = params['resnet_size'] if params and params.has_key('resnet_size') else 32
    # num_blocks = (resnet_size-2)//6

    with tf.variable_scope('initial_conv'):
        inputs = tf.layers.conv2d(inputs=inputs, filters=64, kernel_size=3,
                                  strides=2, padding='SAME')

    inputs = tf.layers.max_pooling2d(
        inputs=inputs, pool_size=3, strides=2, padding='SAME')

    inputs = block_layer(
        inputs=inputs, filters=64, block_fn=bottleneck_block, blocks=3,
        strides=1, is_training=is_training, name='block_layer1')
    inputs = block_layer(
        inputs=inputs, filters=128, block_fn=bottleneck_block, blocks=4,
        strides=2, is_training=is_training, name='block_layer2')
    inputs = block_layer(
        inputs=inputs, filters=256, block_fn=bottleneck_block, blocks=23,
        strides=2, is_training=is_training, name='block_layer3')
    inputs = block_layer(
        inputs=inputs, filters=512, block_fn=bottleneck_block, blocks=3,
        strides=2, is_training=is_training, name='block_layer4')

    with tf.variable_scope('BN'):
        inputs = utils.bn_relu(inputs, is_training)
        tf.summary.scalar('sparsity', tf.nn.zero_fraction(inputs))
    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.identity(inputs, 'final_avg_pool')
    # inputs = tf.nn.dropout(inputs, 0.5 if is_training else 1.)
    inputs = tf.layers.dense(inputs, num_classes)
    inputs = tf.identity(inputs, 'final_dense')
    return inputs

def inference_resnet32(inputs, num_classes, is_training, params=None):
    resnet_size = params['resnet_size'] if params and params.has_key('resnet_size') else 32
    num_blocks = (resnet_size-2)//6

    with tf.variable_scope('initial_conv'):
        inputs = tf.layers.conv2d(inputs=inputs, filters=16, kernel_size=3,
                                  strides=1, padding='SAME')

    inputs = block_layer(
        inputs=inputs, filters=16, block_fn=building_block, blocks=num_blocks,
        strides=1, is_training=is_training, name='block_layer1')
    inputs = block_layer(
        inputs=inputs, filters=32, block_fn=building_block, blocks=num_blocks,
        strides=2, is_training=is_training, name='block_layer2')
    inputs = block_layer(
        inputs=inputs, filters=64, block_fn=building_block, blocks=num_blocks,
        strides=2, is_training=is_training, name='block_layer3')

    with tf.variable_scope('BN'):
        inputs = utils.bn_relu(inputs, is_training)
        tf.summary.scalar('sparsity', tf.nn.zero_fraction(inputs))
    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.identity(inputs, 'final_avg_pool')
    # inputs = tf.nn.dropout(inputs, 0.5 if is_training else 1.)
    inputs = tf.layers.dense(inputs, num_classes)
    inputs = tf.identity(inputs, 'final_dense')
    return inputs


def inference_test(inputs, num_classes, is_training, params=None):
    resnet_size = params['resnet_size'] if params and params.has_key('resnet_size') else 32
    num_blocks = (resnet_size-2)//6

    with tf.variable_scope('initial_conv'):
        inputs = tf.layers.conv2d(inputs=inputs, filters=16, kernel_size=3,
                                  strides=1, padding='SAME')

    inputs = block_layer(
        inputs=inputs, filters=16, block_fn=building_block, blocks=num_blocks,
        strides=1, is_training=is_training, name='block_layer1')
    inputs = block_layer(
        inputs=inputs, filters=32, block_fn=building_block, blocks=num_blocks,
        strides=2, is_training=is_training, name='block_layer2')


    with tf.variable_scope('BN'):
        inputs = utils.bn_relu(inputs, is_training)
        tf.summary.scalar('sparsity', tf.nn.zero_fraction(inputs))
    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.identity(inputs, 'final_avg_pool')
    # inputs = tf.nn.dropout(inputs, 0.5 if is_training else 1.)
    inputs = tf.layers.dense(inputs, num_classes)
    inputs = tf.identity(inputs, 'final_dense')
    return inputs