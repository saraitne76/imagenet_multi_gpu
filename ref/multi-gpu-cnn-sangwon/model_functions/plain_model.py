import tensorflow as tf
import utils


def inference_plain_nodrop(inputs, num_classes, is_training, params=None):
    inputs = tf.layers.conv2d(inputs=inputs, filters=64, kernel_size=11, padding='SAME', name='conv1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 2, 2)

    inputs = tf.layers.conv2d(inputs=inputs, filters=128, kernel_size=9, padding='SAME', name='conv2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)

    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)

    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-2')
    inputs = utils.bn_relu(inputs, is_training)

    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.layers.dense(inputs, activation=tf.nn.relu, units=100)
    inputs = tf.layers.dropout(inputs, 0.7 if is_training else 1.)
    inputs = tf.layers.dense(inputs, num_classes)
    return inputs


def inference_plain_channeldrop(inputs, num_classes, is_training, params=None):
    inputs = tf.layers.conv2d(inputs=inputs, filters=64, kernel_size=11, padding='SAME', name='conv1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 2, 2)

    inputs = tf.layers.conv2d(inputs=inputs, filters=128, kernel_size=9, padding='SAME', name='conv2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)

    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)

    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-2')
    inputs = utils.bn_relu(inputs, is_training)

    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.layers.dense(inputs, activation=tf.nn.relu, units=100)
    inputs = tf.layers.dropout(inputs, 0.7 if is_training else 1.)
    inputs = tf.layers.dense(inputs, num_classes)
    return inputs


def inference_plain_normaldrop(inputs, num_classes, is_training, params=None):
    keep_prob = 0.7 if is_training else 1.
    inputs = tf.layers.conv2d(inputs=inputs, filters=64, kernel_size=11, padding='SAME', name='conv1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 2, 2)
    inputs = tf.nn.dropout(inputs, keep_prob)

    inputs = tf.layers.conv2d(inputs=inputs, filters=128, kernel_size=9, padding='SAME', name='conv2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)
    inputs = tf.nn.dropout(inputs, keep_prob)

    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.nn.dropout(inputs, keep_prob)
    inputs = tf.layers.conv2d(inputs=inputs, filters=256, kernel_size=5, padding='SAME', name='conv3-2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.layers.average_pooling2d(inputs, 5, 5)
    inputs = tf.nn.dropout(inputs, keep_prob)

    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-1')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.nn.dropout(inputs, keep_prob)
    inputs = tf.layers.conv2d(inputs=inputs, filters=512, kernel_size=3, padding='SAME', name='conv4-2')
    inputs = utils.bn_relu(inputs, is_training)
    inputs = tf.nn.dropout(inputs, keep_prob)

    inputs = tf.reduce_mean(inputs, axis=[1, 2])
    inputs = tf.layers.dense(inputs, activation=tf.nn.relu, units=100)
    inputs = tf.layers.dropout(inputs, keep_prob)
    inputs = tf.layers.dense(inputs, num_classes)
    return inputs
