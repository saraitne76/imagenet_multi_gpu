import tensorflow as tf
import os
import time

from datetime import datetime
from input_functions import dataset
from model_functions import resnet_model
from model_functions import plain_model

tf.app.flags.DEFINE_string('model_dir', 'plain_nodrop',
                           'Training data directory')

tf.app.flags.DEFINE_string('dataset_name', 'DCASE_1A',
                           'Dataset to use in training')
tf.app.flags.DEFINE_integer('num_gpus', 2,
                            'Number of gpus to use.')

tf.app.flags.DEFINE_integer('batch_size', 64,
                            'define batch size')

tf.app.flags.DEFINE_integer('time_per_eval', 1, 'interval of evaluation')

tf.app.flags.DEFINE_boolean('log_acc_top1', True, '')
tf.app.flags.DEFINE_boolean('log_acc_top5', True, '')
tf.app.flags.DEFINE_boolean('log_kappa', False, '')
FLAGS = tf.app.flags.FLAGS

net = plain_model

def main(_):
    last_model = -1

    os.environ["CUDA_VISIBLE_DEVICES"] = "0, 1"
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    with tf.device('/cpu:0'):
        eval_dataset = dataset.Dataset(FLAGS.dataset_name, False)
        steps_per_epoch = eval_dataset.get_number_of_examples()/FLAGS.batch_size
        if eval_dataset.get_number_of_examples()%FLAGS.batch_size ==0:
            steps_per_epoch-=1
        print('%s: Dataset: %s / %d steps per epoch' %
              (str(datetime.now())[:-7], FLAGS.dataset_name, steps_per_epoch))

        config = tf.ConfigProto(allow_soft_placement = True)
        config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)

        model_path = os.path.join('./models', FLAGS.dataset_name, FLAGS.model_dir)
        ckpt_path = os.path.join(model_path, 'train')
        summary_writer = tf.summary.FileWriter(os.path.join(model_path, 'eval'), graph=sess.graph)

        # imgs, labels = train_dataset.get_input_fn(False, FLAGS.batch_size)
        iterator = eval_dataset.get_input_fn(FLAGS.batch_size)
        imgs, labels = iterator.get_next()
        labels = tf.squeeze(labels, -1)

        num_classes = eval_dataset.get_num_classes()
        num_examples = eval_dataset.get_number_of_examples()

        split_imgs = tf.split(imgs, FLAGS.num_gpus)
        split_labels = tf.split(labels, FLAGS.num_gpus)

        full_logits = []
        cross_entropys = []
        l2norms = []
        total_loss = []
        reuse = None
        for i in range(FLAGS.num_gpus):
            with tf.device('/gpu:%d' % i):
                with tf.name_scope('tower_%d'% (i)) as scope:
                    print('%s: building tower %d' %(datetime.now(), i))
                    with tf.variable_scope(tf.get_variable_scope(), reuse=reuse):
                        # loss = get_loss(split_imgs[i], split_labels[i], num_classes, scope, reuse=reuse)
                        # logits = net.inference(split_imgs[i], num_classes, is_training=False)
                        logits = net.inference_plain_nodrop(split_imgs[i], num_classes, is_training=False)
                        full_logits.append(logits)

                    cross_entropy = tf.losses.softmax_cross_entropy(
                        tf.one_hot(split_labels[i], num_classes), logits, scope=scope)
                    cross_entropys.append(cross_entropy)

                    # r_loss = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                    l2norm = 5e-4 * tf.add_n(
                        [tf.nn.l2_loss(v) for v in tf.trainable_variables()])
                    l2norms.append(l2norm)
                    total_losses = cross_entropy
                    total_loss.append(total_losses)

                    reuse = True

        cross_entropys = tf.metrics.mean(cross_entropys)
        l2norms = tf.metrics.mean(l2norms)
        total_loss = tf.metrics.mean(total_loss)
        losses_op = [cross_entropys[1], l2norms[1], total_loss[1]]
        logits = tf.concat(full_logits, axis=0)

        log_vals_ops = []
        met_vals_ops = []
        base_form = '%s %d step, loss: %.6f, duration: %.2f'
        log_form = ''
        with tf.name_scope('accuracy'):
            if FLAGS.log_acc_top1:
                acc_t1 = tf.metrics.mean(tf.cast(
                        tf.nn.in_top_k(logits, labels, 1), tf.float32))
                tf.summary.scalar('top1', acc_t1[0])
                log_vals_ops.append(acc_t1[1])
                met_vals_ops.append(acc_t1[0])
                log_form = log_form + ', top1: %.4f'
            if FLAGS.log_acc_top5:
                acc_t5 = tf.metrics.mean(tf.cast(
                        tf.nn.in_top_k(logits, labels, 5), tf.float32))
                tf.summary.scalar('top5', acc_t5[0])
                log_vals_ops.append(acc_t5[1])
                met_vals_ops.append(acc_t5[0])
                log_form = log_form + ', top5: %.4f'

        with tf.name_scope('kappa'):
            if FLAGS.log_kappa:
                cohen_kappa = tf.contrib.metrics.cohen_kappa(
                    labels, tf.argmax(logits, axis=1), num_classes=num_classes)
                tf.summary.scalar('kappa', cohen_kappa[1])
                log_vals_ops.append(cohen_kappa[1])
                met_vals_ops.append(cohen_kappa[0])
                log_form = log_form + ', kappa: %.4f'

        with tf.name_scope('losses'):
            tf.summary.scalar('xentropy', cross_entropys[0])
            tf.summary.scalar('l2norm', l2norms[0])
            tf.summary.scalar('total', cross_entropys[0]+l2norms[0])

        summary = tf.summary.merge_all()

        while 1:
            try:
                recent_model = open(ckpt_path+'/checkpoint', 'r').readline().split('"')[1]
            except IOError:
                time.sleep(FLAGS.time_per_eval)
                continue

            if last_model==recent_model:
                time.sleep(FLAGS.time_per_eval)
                continue

            print('evaluate on %s' % recent_model)
            last_model = recent_model


            model_path = os.path.join('./models', FLAGS.dataset_name, FLAGS.model_dir, 'train')
            recent_model = open(os.path.join(model_path, 'checkpoint'), 'r').readline().split('"')[1]
            restorer = tf.train.Saver()
            restorer.restore(sess, os.path.join(model_path, recent_model))

            init = [iterator.initializer, tf.local_variables_initializer()]
            sess.run(init)

            for step in range(int(steps_per_epoch)):
                start_time = time.time()
                try:
                    losses, vals_pack = sess.run([losses_op, log_vals_ops])

                    if step%10==0:
                        duration = time.time() - start_time
                        print(base_form %
                              (str(datetime.now())[:-7], step, losses[-1], duration)),
                        print(log_form %
                              tuple(vals_pack))

                except tf.errors.OutOfRangeError:
                    sess.run(init)
                    pass

            vals_pack = sess.run(met_vals_ops)
            print(log_form[2:] % tuple(vals_pack))
            print
            summary_writer.add_summary(sess.run(summary), global_step=int(recent_model[11:]))


        print('breaked')

if __name__ == '__main__':
    tf.app.run()