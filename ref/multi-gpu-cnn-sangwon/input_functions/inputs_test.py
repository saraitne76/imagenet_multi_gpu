import dataset
import tensorflow as tf
import numpy as np
import cv2

dataset = dataset.Dataset('cifar10', True)

iterator = dataset.get_input_fn(1)
with tf.Session() as sess:
    sess.run(iterator.initializer)
    imgs, labels = iterator.get_next()
    # while 1:
    for i in range(6):
        img, label = sess.run([imgs, labels])
        # if index.shape!=(64, 64, 3):
        #     print index.shape
        img = np.array(img*255)[...,[2, 1, 0]]
        print img.shape
        cv2.imwrite('test.png', img[0])