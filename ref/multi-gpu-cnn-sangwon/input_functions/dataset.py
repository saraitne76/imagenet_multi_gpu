import tensorflow as tf

dataset_dict = {
    # prefix dataset dictionary with
    # 'name': [/path/and/format/of/train, /path/and/foramt/of/validation,
    #           number of shard of train, number of shard of validation,
    #           train_samples, eval_samples,
    #           num_classes, height, width, channel]

    'test': ['/home/swlee/data/test/test/train-%.4d-of-%.4d',
             '/home/swlee/data/test/test/validation-%.4d-of-%.4d',
             4, 4, 61382, 4090, 818, 32, 32, 3],

    'tiny-imagenet': ['/home/swlee/data/tiny-imagenet-200/tfr/train-%.4d-of-%.4d',
                      '/home/swlee/data/tiny-imagenet-200/tfr/validation-%.4d-of-%.4d',
                      200, 10, 100000, 10000, 200, 64, 64, 3],
    'tiny-imagenet-10': ['/home/swlee/data/tiny-imagenet-10/tfr/train-%.4d-of-%.4d',
                         '/home/swlee/data/tiny-imagenet-10/tfr/validation-%.4d-of-%.4d',
                         10, 5, 5000, 500, 10, 64, 64, 3],
    'cifar10': ['/home/swlee/data/cifar10_data/tfr/train-%.4d-of-%.4d',
                '/home/swlee/data/cifar10_data/tfr/validation-%.4d-of-%.4d',
                100, 10, 50000, 10000, 11, 32, 32, 3],
    'MURA': ['/DATA2/MURA-v1.1/tfr/train-%.4d-of-%.4d',
             '/DATA2/MURA-v1.1/tfr/validation-%.4d-of-%.4d',
             100, 10, 36808, 3197, 2, 512, 512, 1],
    'DCASE_1A': ['/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development/tfr/train-%.4d-of-%.4d',
                 '/DATA2/DCASE/TUT-urban-acoustic-scenes-2018-development/tfr/validation-%.4d-of-%.4d',
                 10, 5, 6122, 2518, 10, 249, 250, 1],
}

class Dataset:
    def __init__(self, dataset_name, is_training):
        self.dataset = dataset_name
        self.is_training = is_training

        if dataset_dict.has_key(self.dataset):
            self.dict = dataset_dict[self.dataset]

    def _get_dir(self):
        if self.is_training:
            num_shard = self.dict[2]
            return [self.dict[0] % (x+1, num_shard) for x in range(num_shard)]
        else:
            num_shard = self.dict[3]
            return [self.dict[1] % (x+1, num_shard) for x in range(num_shard)]

    def _data_parser(self, example):
        features = {
            'data/encoded': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
            'data/class/label': tf.FixedLenFeature([1], dtype=tf.int64, default_value=-1),
            # 'data/class/text': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
            # 'data/format': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
            # 'data/shape': tf.FixedLenFeature([], dtype=tf.string, default_value='')
        }
        features = tf.parse_single_example(example, features)

        # data = tf.cast(tf.image.decode_image(features['data/encoded']), tf.float32)
        data = tf.cast(tf.decode_raw(features['data/encoded'], tf.float64), tf.float32)
        data = tf.reshape(data, self.dict[7:10])
        # shape = tf.cast(tf.decode_raw(features['data/shape'], tf.int64), tf.int32)
        label = tf.cast(features['data/class/label'], dtype=tf.int32)
        return data/100, tf.reshape(label, [-1])
        # return data/255, tf.reshape(label, [-1])

    def _train_preprocess(self, img, label):
        img = tf.image.resize_image_with_crop_or_pad(img, self.dict[7]+8, self.dict[8]+8)
        img = tf.random_crop(img, self.dict[7:10])
        img = tf.image.random_flip_left_right(img)
        # img = tf.image.rotate(img, tf.random_uniform([1], 0, 10))
        return img, label

    def _eval_preprocess(self, img, label):
        img = tf.image.resize_image_with_crop_or_pad(img, self.dict[7], self.dict[8])
        img = tf.reshape(img, self.dict[7:10])
        return img, label

    def _prepreocess(self, img, label):
        img = tf.pad(img, [[0, 1], [0, 0], [0, 0]])
        # img = tf.image.resize_images(img, [224, 224])
        # img = tf.image.resize_image_with_crop_or_pad(img, self.dict[7]+1, self.dict[8])
        # img = tf.image.per_image_standardization(img)
        return img, label

    def _input_fn(self, data_dir, batch_size, num_epochs, augmentation):

        dataset = tf.data.TFRecordDataset(data_dir)

        dataset = dataset.map(self._data_parser, num_parallel_calls=3)

        if self.is_training:
        #     dataset = dataset.map(self._train_preprocess, num_parallel_calls=3)
            dataset = dataset.shuffle(buffer_size=10000)
        # else:
        #     dataset = dataset.map(self._eval_preprocess, num_parallel_calls=3)

        dataset = dataset.map(self._prepreocess, num_parallel_calls=3)

        dataset = dataset.repeat(num_epochs)
        iterator = dataset.batch(batch_size).make_initializable_iterator()
        return iterator

    def get_input_fn(self, batch_size, num_epoch=1, augmentation=None):
        return self._input_fn(self._get_dir(), batch_size, num_epoch, augmentation)

    def get_name(self):
        return self.dataset

    def get_number_of_examples(self):
        index = 4 if self.is_training else 5
        return self.dict[index]

    def get_num_classes(self):
        return self.dict[6]