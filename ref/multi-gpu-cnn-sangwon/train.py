import tensorflow as tf
import os
import time

from datetime import datetime
from input_functions import dataset
from model_functions import resnet_model
from model_functions import plain_model

tf.app.flags.DEFINE_string('model_dir', 'plain_nodrop',
                           'Training data directory')

tf.app.flags.DEFINE_string('dataset_name', 'DCASE_1A',
                           'Dataset to use in training')
tf.app.flags.DEFINE_boolean('use_pretrain_model', False,
                            'if specified, restore pretrained model')
tf.app.flags.DEFINE_string('pretrain_model_path', '',
                            'if specified, restore pretrained model from this path')
tf.app.flags.DEFINE_integer('num_gpus', 2,
                            'Number of gpus to use.')

tf.app.flags.DEFINE_integer('batch_size', 32,
                            'define batch size')
tf.app.flags.DEFINE_integer('epochs_per_decay', 80,
                            'define batch size')
tf.app.flags.DEFINE_integer('steps_per_save', 200,
                            'define batch size')

tf.app.flags.DEFINE_float('initial_learning_rate', 0.025,
                          'inital learning rate')
tf.app.flags.DEFINE_float('lr_decay', 0.2,
                          'learning rate decay factor')
tf.app.flags.DEFINE_integer('max_epochs', 400,
                            'max epochs')

tf.app.flags.DEFINE_boolean('log_acc_top1', True, '')
tf.app.flags.DEFINE_boolean('log_acc_top5', True, '')
tf.app.flags.DEFINE_boolean('log_kappa', False, '')
FLAGS = tf.app.flags.FLAGS

net = plain_model

def merge_gradients(temp_grads):
    average_grads = []
    for grad_and_vars in zip(*temp_grads):
        grads = []
        for g, _ in grad_and_vars:
            expanded_g = tf.expand_dims(g, 0)
            grads.append(expanded_g)
        grad = tf.concat(grads, axis=0)
        grad = tf.reduce_mean(grad, 0)

        v = grad_and_vars[0][1]
        grad_and_vars = (grad, v)
        average_grads.append(grad_and_vars)
    return average_grads


def main(_):
    with tf.device('/cpu:0'):
        #   set visible gpu devices and
        #   block tensorflow's basic log
        os.environ["CUDA_VISIBLE_DEVICES"] = "0, 1"
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        #   create session
        config = tf.ConfigProto(allow_soft_placement = True)
        config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)

        #   create global_step variable
        #   in case of using pretrained model, initialize with restart point
        global_step_init = 0
        if FLAGS.use_pretrain_model and FLAGS.pretrain_model_path:
            global_step_init = int(open(os.path.join(FLAGS.pretrain_model_path, 'checkpoint'), 'r')
                                   .readline().split('-')[1])

        global_step = tf.get_variable('global_step', [],
                                      initializer=tf.constant_initializer(global_step_init), trainable=False)

        #   build dataset object, and get some information and
        #   input batchs
        train_dataset = dataset.Dataset(FLAGS.dataset_name, True)
        steps_per_epoch = train_dataset.get_number_of_examples()/FLAGS.batch_size + 1
        if train_dataset.get_number_of_examples()%FLAGS.batch_size !=0:
            steps_per_epoch+=1
        print('%s: Dataset: %s / %d steps per epoch' %
              (str(datetime.now())[:-7], FLAGS.dataset_name, steps_per_epoch))

        iterator = train_dataset.get_input_fn(FLAGS.batch_size, num_epoch=5)
        imgs, labels = iterator.get_next()
        labels = tf.squeeze(labels, -1)
        num_classes = train_dataset.get_num_classes()

        #   split input batchs into num_gpus
        split_imgs = tf.split(imgs, FLAGS.num_gpus)
        split_labels = tf.split(labels, FLAGS.num_gpus)

        #   make learing_rate variable with lr decay policy
        decay_steps = int(steps_per_epoch * FLAGS.epochs_per_decay)
        learning_rate = tf.train.exponential_decay(FLAGS.initial_learning_rate, global_step,
                                                   decay_steps, FLAGS.lr_decay, staircase=True)
        tf.summary.scalar('learning_rate', learning_rate)

        #   choose optimizer for model
        optimizer = tf.train.MomentumOptimizer(learning_rate, momentum=0.9)
        # optimizer = tf.train.AdamOptimizer(learning_rate)

        #   build tower(s) to compute logits and losses
        #   reuse should be None for first tower, and True for others
        #   to make sure that each tower shares same weights
        grads = []
        full_logits = []
        cross_entropys = []
        l2norms = []
        reuse = None
        for i in range(FLAGS.num_gpus):
            with tf.device('/gpu:%d' % i):
                with tf.name_scope('tower_%d'% (i)) as scope:
                    print('%s: building tower %d' %(str(datetime.now())[:-7], i))
                    with tf.variable_scope(tf.get_variable_scope(), reuse=reuse):
                        # logits = net.inference(split_imgs[i], num_classes, is_training=True)
                        logits = net.inference_plain_nodrop(split_imgs[i], num_classes, is_training=True)
                        full_logits.append(logits)

                    cross_entropy = tf.losses.softmax_cross_entropy(
                        tf.one_hot(split_labels[i], num_classes), logits, scope=scope
                        # ,weights=tf.reduce_sum(tf.one_hot(split_labels[i], 2)*[[1., 1.475]], axis=1)
                    )
                    cross_entropys.append(cross_entropy)

                    l2norm = 0
                    # if reuse==None:
                    l2norm = 5e-4 * tf.add_n(
                        [tf.nn.l2_loss(v) for v in tf.trainable_variables()])
                    l2norms.append(l2norm)

                    #   for update moving mean/variance of BN
                    #   add dependencies to optimizer
                    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope=scope)
                    # for op in update_ops:
                    #     print op
                    with tf.control_dependencies(update_ops):
                        loss = tf.add(cross_entropy, l2norm)

                        grad = optimizer.compute_gradients(loss)
                        grads.append(grad)
                    reuse = True

        #   merging logits and gradients from each tower
        print('%s: merging tower(s)' % str(datetime.now())[:-7])
        logits = tf.concat(full_logits, axis=0)
        grads = merge_gradients(grads)
        train_op = optimizer.apply_gradients(grads, global_step=global_step)
            #   == optimizer.minimize(loss)

        #   define some values to score and
        #   add to tensorboard summary
        log_vals_ops = []
        base_form = '%s %d step, lr: %s, loss: %.6f, duration: %.2f'
        log_form = ''

        with tf.variable_scope('accuracy'):
            if FLAGS.log_acc_top1:
                acc_t1 = tf.reduce_mean(tf.cast(
                        tf.nn.in_top_k(logits, labels, 1), tf.float32))
                tf.summary.scalar('top1', acc_t1)
                log_vals_ops.append(acc_t1)
                log_form = log_form + ', top1: %.4f'
            if FLAGS.log_acc_top5:
                acc_t5 = tf.reduce_mean(tf.cast(
                        tf.nn.in_top_k(logits, labels, 5), tf.float32))
                tf.summary.scalar('top5', acc_t5)
                log_vals_ops.append(acc_t5)
                log_form = log_form + ', top5: %.4f'

        with tf.name_scope('kappa'):
            if FLAGS.log_kappa:
                cohen_kappa = tf.contrib.metrics.cohen_kappa(
                    labels, tf.argmax(logits, axis=1), num_classes=num_classes)
                tf.summary.scalar('kappa', cohen_kappa[1])
                log_vals_ops.append(cohen_kappa[1])
                log_form = log_form + ', kappa: %.4f'

        with tf.name_scope('losses'):
            tf.summary.scalar('xentropy', tf.reduce_mean(cross_entropys))
            tf.summary.scalar('l2norm', tf.reduce_mean(l2norms))
            tf.summary.scalar('total', tf.reduce_mean(cross_entropys+l2norms))

        #   get the number of total trainable parameters
        params = float(sess.run(sum([tf.shape(tf.reshape(x, [-1]))[0] for x in tf.trainable_variables()])))
        print('%s: total trainable parameters: %.3fM' %
              (str(datetime.now())[:-7], params/1e6))

        #   make saver and summary writer to save model weights and summary
        saver = tf.train.Saver()
        model_path = os.path.join('./models', FLAGS.dataset_name, FLAGS.model_dir, 'train')
        ckpt_path = os.path.join(model_path, 'model.ckpt')
        summary = tf.summary.merge_all()
        summary_writer = tf.summary.FileWriter(model_path, graph=sess.graph)

        #   if FLAGS.use_pretrain_model is set and model path is correct
        #   restore pretrained model from path
        if FLAGS.use_pretrain_model and FLAGS.pretrain_model_path:
            recent_model = open(os.path.join(model_path, 'checkpoint')
                                , 'r').readline().split('"')[1]
            restorer = tf.train.Saver()
            restorer.restore(sess, os.path.join(model_path, recent_model))
            print('%s: model restored from %s' % (datetime.now(), FLAGS.pretrain_model_path))
            print('%s: training restarts at %d' % (datetime.now(), global_step_init))

        #   initialize variables and batch runner
        print('%s: initializing variables' % str(datetime.now())[:-7])
        init = [tf.global_variables_initializer(), tf.local_variables_initializer(), iterator.initializer]
        sess.run(init)

        print('%s: start training' % str(datetime.now())[:-7])
        # op_pack = list([val] for val in log_vals_ops.itervalues())
        # print op_pack
        start_time = time.time()
        for step in range(int(FLAGS.max_epochs*steps_per_epoch)):
            try:
                sess.run(train_op)

                if step%100==0:
                    loss_val, vals_pack = sess.run([loss, log_vals_ops])
                    duration = time.time() - start_time
                    print(base_form %
                          (str(datetime.now())[:-7], step, sess.run(learning_rate), loss_val, duration)),
                    print(log_form %
                          tuple(vals_pack))
                    start_time = time.time()

                    summary_writer.add_summary(sess.run(summary), step)

                if step%FLAGS.steps_per_save==0:
                    saver.save(sess, ckpt_path, global_step=step)

            except tf.errors.OutOfRangeError:
                sess.run([tf.local_variables_initializer(), iterator.initializer])

if __name__ == '__main__':
    tf.app.run()