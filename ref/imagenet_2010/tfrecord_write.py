import tensorflow as tf
from PIL import Image
import os
import random
import threading


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def example_write(tfrecord, examples):
    global count
    writer = tf.python_io.TFRecordWriter(tfrecord)
    for ex in examples:
        img = Image.open(ex[0])
        if img.getbands() != ('R', 'G', 'B'):
            img = img.convert('RGB')
        h, w = img.size
        feature = {'label': _int64_feature(ex[1]),
                   'image': _bytes_feature(img.tobytes()),
                   'height': _int64_feature(h),
                   'width': _int64_feature(w),
                   'file_name': _bytes_feature(tf.compat.as_bytes(ex[0]))}
        example = tf.train.Example(features=tf.train.Features(feature=feature))
        writer.write(example.SerializeToString())
        count = count + 1
    writer.close()


thread_num = 8

path = '/home/miracle/ILSVRC2010/ILSVRC2010_devkit-1.0/devkit-1.0/data/ILSVRC2010_train.txt'
f = open(path)
lines = f.readlines()
f.close()

class2index = {}
for line in lines:
    split = line.split()
    class2index[split[0]] = int(split[1])
num_class = len(class2index)

path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_train/ILSVRC2010_images_train/'
examples = []
train_folder = os.listdir(path)
for folder in train_folder:
    imgs = os.listdir(path + folder)
    label = class2index[folder]
    for img in imgs:
        img_path = path + folder + '/' + img
        examples.append((img_path, label))
random.shuffle(examples)

example_num = len(examples)
split = example_num//thread_num
count = 0
for i in range(thread_num):
    if i == thread_num-1:
        t = threading.Thread(target=example_write,
                             args=('Data/train_%02d.tfrecord' %(i), examples[split*i:]))
    else:
        t = threading.Thread(target=example_write,
                             args=('Data/train_%02d.tfrecord' %(i), examples[split*i:split*(i+1)]))
    t.start()
while count < example_num:
    print('\rTraining Data write: %5.2f%%' % ((count/example_num)*100), end='')
print()


examples = []
path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_val/ILSVRC2010_validation_ground_truth.txt'
f = open(path)
lines = f.readlines()
f.close()
path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_val/val/'
val_imgs = os.listdir(path)
val_imgs.sort()
for i in range(len(val_imgs)):
    img_path = path + val_imgs[i]
    label = int(lines[i])
    examples.append((img_path, label))
random.shuffle(examples)

example_num = len(examples)
split = example_num//thread_num
count = 0
for i in range(thread_num):
    if i == thread_num-1:
        t = threading.Thread(target=example_write,
                             args=('Data/val_%02d.tfrecord' %(i), examples[split*i:]))
    else:
        t = threading.Thread(target=example_write,
                             args=('Data/val_%02d.tfrecord' %(i), examples[split*i:split*(i+1)]))
    t.start()
while count < example_num:
    print('\rValidation Data write: %5.2f%%' % ((count/example_num)*100), end='')
print()


examples = []
path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_test/ILSVRC2010_test_ground_truth'
f = open(path)
lines = f.readlines()
f.close()
path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_test/test/'
test_imgs = os.listdir(path)
test_imgs.sort()
for i in range(len(test_imgs)):
    img_path = path + test_imgs[i]
    label = int(lines[i])
    examples.append((img_path, label))
example_num = len(examples)
split = example_num//thread_num
count = 0
for i in range(thread_num):
    if i == thread_num-1:
        t = threading.Thread(target=example_write,
                             args=('Data/test_%02d.tfrecord' %(i), examples[split*i:]))
    else:
        t = threading.Thread(target=example_write,
                             args=('Data/test_%02d.tfrecord' %(i), examples[split*i:split*(i+1)]))
    t.start()
while count < example_num:
    print('\rTest Data write: %5.2f%%' % ((count/example_num)*100), end='')
print()