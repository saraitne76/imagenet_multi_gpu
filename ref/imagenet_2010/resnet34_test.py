from model import *

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

tf_list = os.listdir('Data/')
tf_list.sort()
tf_list = list(map(lambda a: 'Data/'+a, tf_list))

test_tfrecord = tf_list[0:8]
#train_tfrecord = tf_list[8:16]
#val_tfrecord = tf_list[16:24]

model = 'resnet34'

graph = tf.Graph()
with graph.as_default():
    cnn = CNN(model, 1000, (224, 224, 3))
    cnn.build_eval(model, test_tfrecord)
sess = tf.Session(graph=graph, config=get_tf_config())

uneval_ckpt = get_uneval_ckpt('resnet34', 'Test-log.txt')
for ckpt in uneval_ckpt:
    cnn.test(sess, ckpt)
sess.close()
