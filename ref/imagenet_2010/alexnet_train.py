from model import *

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

tf_list = os.listdir('Data/')
tf_list.sort()
tf_list = list(map(lambda a: 'Data/'+a, tf_list))

#test_tfrecord = tf_list[0:8]
train_tfrecord = tf_list[8:16]
#val_tfreocrd = tf_list[16:24]

model = 'alexnet'

graph = tf.Graph()
with graph.as_default():
    cnn = CNN(model, 1000, (224, 224, 3))
    cnn.build_train(model, train_tfrecord, 128)
sess = tf.Session(graph=graph, config=get_tf_config())
cnn.train(sess, 30, 1e-2)
sess.close()
