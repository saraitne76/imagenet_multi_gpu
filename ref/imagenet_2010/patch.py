import os

patch_path = '/home/miracle/ILSVRC2010/patch_images/train/'
target_path = '/home/miracle/ILSVRC2010/ILSVRC2010_images_train/ILSVRC2010_images_train/'

patch_class = os.listdir(patch_path)

#print(patch_class)

for c in patch_class:
    files = os.listdir(patch_path + c)
    for file in files:
        os.rename(patch_path + c + '/' + file, target_path + c + '/' + file)